"use strict";

// Node http server
var app = require('http').createServer( function (request, response) {
	var path = url.parse(request.url).pathname;
	switch (path) {
		case '/':
			fs.readFile(__dirname + '/index.html', function (error, data) {
				if (error) {
					response.writeHead(500);
					response.end('Error loading index.html - 500');
				} else {
					response.writeHead(200);
					response.end(data);
				}
			});
			break;
		default:
			fs.readFile(__dirname + path, function (error, data) {
				if (error) {
					response.writeHead(404);
					response.end('File not found - 404');
				} else {
					response.writeHead(200);
					response.end(data);
				}
			});
			break;
	}
}).listen(8080);

var io = require('socket.io').listen(app);
var fs = require('fs');
var url = require('url');

// Game code shared by client and server
var coreGame = require('./js/core-game.js');

// Holds our list of games and manages them 
var gamesManager = require('./js/server-side/games-manager.js');

// Socket.io will call this function when a client connects
io.sockets.on('connect', function (client) {
	// Decorate our client with player data
    coreGame.Player(client);

	// Set the client's id 
	client.emit('onConnected', { clientId: client.id });

    // When the clients hits the 'find game' button
	client.on('findGame', function () {
        if (!client.game) {
            // Find or create a game for the client
            var game = gamesManager.findGame();
            
            // Tell the other clients in game that a new player is joining 
            gamesManager.notifyClientsPlayerJoined(game, client);

            // Add the client to the game
            game.addClient(client);

            client.game = game;
        }
    });

    // Start the game
	client.on('startGame', function () {
        if (client.game) {
            // Start the game loops if they're not running
            if (!gamesManager.runGames) {
                gamesManager.runGames = true;
                gamesLoop();
                broadcastGameStateLoop();
            }

            client.game.startGame();
        }
    });

    // Receive a packet from the client
	client.on('clientToServerPacket', function (data) {
        if (client.game) {
            client.game.receiveClientPacket(data.packet);
        }
    });
    
	// When the client disconnects
	client.on('disconnect', function () {
        if (client.game) {
            gamesManager.notifyClientsPlayerLeft(client.game, client);
            client.game.removeClient(client);
        }
	});
});

/* These loops should probably exist within gamesManager */
// Update all running games ~60 times a second
var then = Date.now();
function gamesLoop() {
	var now = Date.now();
	var delta = (now - then) / 1000;
    gamesManager.updateGames(delta);
	then = now;

    if (gamesManager.runGames) {
	    setTimeout(gamesLoop, 16);	
    }
}

// Broadcast game states to all clients
function broadcastGameStateLoop() {
    gamesManager.broadcastGameStates();

    if (gamesManager.runGames) {
    	setTimeout(broadcastGameStateLoop, coreGame.networkDelay);	
    }
}
