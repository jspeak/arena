// Loader for sound and image assets
var loader = {
	loaded: true,
	loadedCount: 0, // Assets loaded so far
	totalCount: 0, // Total number of assets

	// Check for sound support
	init: function () {
		// Creates the <audio> tag
		var audio = document.createElement('audio');
	
		// Check if the browswer supports the <audio> tag
		if (audio.canPlayType) {
			var mp3Support = audio.canPlayType('audio/mpeg');
			var oggSupport = audio.canPlayType('audio/ogg; codecs="vorbis"');
		}
		else {
			// The <audio> tag is not supported		
			mp3Support = false;
			oggSupport = false;
		}

		// Check for ogg, then mp3, or set soundFileExtn to undefined
		if (oggSupport)
			this.soundFileExtn = ".ogg";
		else if (mp3Support)
			this.soundFileExtn = ".mp3";
		else
			this.soundFileExtn = undefined;
	},

	loadImage: function(url) {
		this.totalCount++;
		this.loaded = false;
		$('#loadingscreen').show();
		var image = new Image();
		image.onload = this.itemLoaded;
		image.src = url;
		return image;
	},

	loadSound: function(url) { 
		this.totalCount++;
		this.loaded = false;
		$('#loadingscreen').show();
		var audio = new Audio();
		audio.src = url + this.soundFileExtn;
		audio.addEventListener("canplaythrough", this.itemLoaded, false);
		return audio;
	},

	itemLoaded: function() {
		this.loadedCount++;
		$('#loadingmessage').html('Loaded ' + this.loadedCount + ' of ' + this.totalCount);
		if (this.loadedCount === this.totalCount) {
			// Loader has fully loaded	
			this.loaded = true;
			$('#loadingscreen').hide();

			// Call the loader onload funciton if it exists
			if (this.onload) {
				this.onload();
				this.onload = undefined;
			}
		}
	}
}
