"use strict";

// Client specific game code
var clientGame = (function() {
    // Private members 
    var id = '';
    var input = []; // Input obtained from the keyboard
    var pendingInstructions = []; // Instructions the server has yet to process
	var instructionSequenceNumber = 0; 

    // Update the game from our received packets 
    function updateGameState(delta) {
        // We need to wait for at least two packets
        if (clientGame.serverPackets.length < 2) {
            // Execute input for mainPlayer while we wait
            predictClient(delta);

            return;
        }
        
        // Get our two most recent packets
        var pastGameState = clientGame.serverPackets[clientGame.serverPackets.length-2];
        var targetGameState = clientGame.serverPackets[clientGame.serverPackets.length-1];

        // Calculate how "far in" we are between the last and next server update 
        var normal = calculateNormalValue(coreGame.networkDelay);

        // Sync main player's position with the server
        var mainPlayerData = getPlayerDataFromPacket(targetGameState, clientGame.mainPlayer.id);
        setCoordinates(clientGame.mainPlayer, mainPlayerData.x, mainPlayerData.y);
        // Play the death sound on death
        if (mainPlayerData.playDeathSound) {
            soundPlayer.playDeathSound();
        }

        clientGame.mainPlayer.kills = mainPlayerData.kills;
        clientGame.mainPlayer.isAlive = mainPlayerData.isAlive;

        // Predict the position of the main player for a lagless experience
        predictClient(delta);

        // Update other players position
        for (var i=0; i<clientGame.otherPlayers.length; i++) {
            var player = clientGame.otherPlayers[i];

            // The player's past data
            var previousPlayerData = getPlayerDataFromPacket(pastGameState, player.id);

            // The player's target data
            var targetPlayerData = getPlayerDataFromPacket(targetGameState, player.id);

            // If we have enough data for the player, update their position and projectiles
            // We interpolate between the two last known game states, neat stuff
            // Don't interpolate interpolate their posistion if it's the start of a new round
            if (previousPlayerData && targetPlayerData) {
                // Update player's position
                if (!targetGameState.newRoundStarted) {
                    setCoordinates(
                        player,
                        Math.round(lerp(previousPlayerData.x, targetPlayerData.x, normal)), // x
                        Math.round(lerp(previousPlayerData.y, targetPlayerData.y, normal)) // y
                    );
                // Don't interpolate if a new round has started
                } else {
                    setCoordinates(
                        player,
                        targetPlayerData.x, // x
                        targetPlayerData.y // y
                    );
                }
                player.isAlive = targetPlayerData.isAlive;
                player.kills = targetPlayerData.kills;

                updateOtherPlayerAnimation(clientGame.otherPlayers[i], previousPlayerData, targetPlayerData);
                playPlayerSounds(targetPlayerData);
            }
        }

        // Update projectiles
        // Copy pastGameState projectiles into clientGame's projectiles
        clientGame.projectiles = [];
        for (var i=0; i<pastGameState.projectiles.length; i++) {
            clientGame.projectiles.push(jQuery.extend({}, pastGameState.projectiles[i]));
        }
        // Interpolate between the two last known positions for each projectile
        for (var i=0; i<clientGame.projectiles.length; i++) {

            var pastProjectile = getProjectileFromPacket(pastGameState, clientGame.projectiles[i].id);
            var targetProjectile = getProjectileFromPacket(targetGameState, clientGame.projectiles[i].id);

            if (targetProjectile) {
                setCoordinates(
                    clientGame.projectiles[i],
                    Math.round(lerp(pastProjectile.x, targetProjectile.x, normal)), // x
                    Math.round(lerp(pastProjectile.y, targetProjectile.y, normal)) // y
                );
            }
        }
    }

    // Store input from the keyboard
    function readInput() {
        input = [];

        // Up-arrow or w
        if (clientGame.keysDown[38] || clientGame.keysDown[87]) { 
            input.push('up');
        }
        // Down-arrow or s
        if (clientGame.keysDown[40] || clientGame.keysDown[83]) {
            input.push('down');
        }
        // Left-arrow or a
        if (clientGame.keysDown[37] || clientGame.keysDown[65]) {
            input.push('left');
        }
        // Right-arrow or d
        if (clientGame.keysDown[39] || clientGame.keysDown[68]) {
            input.push('right');
        }
        // Spacebar 
        if (clientGame.keysDown[32]) {
            input.push('fire');
        }
    } 

    // Update the sprite for the main player
    function updateMainPlayerAnimation() {
        var player = clientGame.mainPlayer;
        if (!player.isAlive) {
            player.setCurrentAnimation(player.animations.dead);
            return;
        }

        // Set starting animation if new round
        var targetGameState = clientGame.serverPackets[clientGame.serverPackets.length-1];
        if (targetGameState.newRoundStarted) {
            var targetPlayerData = getPlayerDataFromPacket(targetGameState, player.id);
            player.setCurrentAnimation(targetPlayerData.currentAnimation);
        }

        // Get last movement input
        for (var i=input.length-1; i>=0; i--) {
            if (input[i] !== 'fire') {
                var lastInput = input[i];
                break;
            }
        }
        
        // Update the sprite animation
        if (lastInput) {
            player.advanceFrameIndex();
        }

        switch (lastInput) {
            case 'up':
                player.setCurrentAnimation(player.animations.moveUp)
                break;
            case 'down':
                player.setCurrentAnimation(player.animations.moveDown);
                break;
            case 'left':
                player.setCurrentAnimation(player.animations.moveLeft);
                break;
            case 'right':
                player.setCurrentAnimation(player.animations.moveRight);
                break;
        }
    }

    // Update the sprites for all other players
    function updateOtherPlayerAnimation(player, previousPlayerData, targetPlayerData) {
        if (!player.isAlive) {
            player.setCurrentAnimation(player.animations.dead);
            return;
        }

        // Manually set player animation if a new round has started
        var targetGameState = clientGame.serverPackets[clientGame.serverPackets.length-1];
        if (targetGameState.newRoundStarted) {
            player.setCurrentAnimation(targetPlayerData.currentAnimation);
        } else {
            // If the player has moved
            if (previousPlayerData.x != targetPlayerData.x || previousPlayerData.y != targetPlayerData.y) {
                player.advanceFrameIndex();

                // Calculate the angle from their last position to their current position
                var angle = Math.round(calculateAngle(
                    previousPlayerData.x,
                    previousPlayerData.y,
                    targetPlayerData.x,
                    targetPlayerData.y
                ) * 100) / 100; // Round to 2 decimal places

                // Use the angle to determine what direction they are facing 
                if (angle > -2.36 && angle < -0.79) { // Up
                    player.setCurrentAnimation(player.animations.moveUp);
                } else if (angle >= -0.79 && angle <= 0.79) { // Right
                    player.setCurrentAnimation(player.animations.moveRight);
                } else if (angle > 0.79 && angle < 2.36) { // Down
                    player.setCurrentAnimation(player.animations.moveDown);
                } else if ((angle >= 2.36 && angle <= 3.14) || angle <=-2.36 ) { // Left
                    player.setCurrentAnimation(player.animations.moveLeft);
                } 
            }
        }
    }

    function playPlayerSounds(targetPlayerData) {
        if (targetPlayerData.playFireSound) {
            soundPlayer.playFireSound();
        }
        if (targetPlayerData.playDeathSound) {
            soundPlayer.playDeathSound();
        }
    }

    // Predict the state of the game for a lagless experience 
    function predictClient(delta) {
        // Predict movement if still alive
        if (clientGame.mainPlayer.isAlive) {
            for (var i=0; i<pendingInstructions.length; i++) {
                for (var j=0; j<pendingInstructions[i].input.length; j++) {
                    coreGame.handleInput(clientGame.mainPlayer, pendingInstructions[i].input[j], delta);
                }
                coreGame.handleProjectileMovement(clientGame.mainPlayer, delta);
                coreGame.handleProjectileCollision(clientGame.mainPlayer, [clientGame.mainPlayer]);
            }
        }
    }
    
    // Keep track of which instructions the server has yet to process
    function updatePendingInstructions() {
        // Index the current instruction
        if (input.length) {
            pendingInstructions.push({ input: input, isn: ++instructionSequenceNumber });
        } 

        // Remove all instructions the server has processed
        var lastPacket = clientGame.serverPackets[clientGame.serverPackets.length-1];
        // Make sure we have a least one packet
        if (lastPacket) {
            var mainPlayerData = getPlayerDataFromPacket(lastPacket, clientGame.mainPlayer.id);

            for (var i=pendingInstructions.length-1; i>=0; i--) {
                if (pendingInstructions[i].isn <= mainPlayerData.lastInstructionSequenceNumber) {

                    pendingInstructions.splice(i, 1);
                } 
            }
        }
    }

    // Prepare a packet to be sent to the server
    function preparePacket() {
        if (input.length) {
            var packet = {
                gameId: clientGame.id,
                clientId: clientGame.mainPlayer.id, 
                instructions: { input: input, isn: instructionSequenceNumber }, 
                time: Date.now() 
            }

            return packet;
        }		
    }

    // Send our packet to the server
    function sendPacket(packet) {
        socket.emit('clientToServerPacket', { packet: packet });
    }

    // Return specified player data from a server packet 
    function getPlayerDataFromPacket(packet, playerId) {
        for (var i=0; i<packet.players.length; i++) {

            if (packet.players[i].id === playerId) {

                return packet.players[i];
            } 
        }
    }

    // Return specified projectile data from a server packet 
    function getProjectileFromPacket(packet, projectileId) {
        for (var i=0; i<packet.projectiles.length; i++) {

            if (packet.projectiles[i].id === projectileId) {

                return packet.projectiles[i];
            } 
        }
    }

    // Set coordinates for a player or projectile
    function setCoordinates(entity, x, y) {
        entity.x = x;
        entity.y = y;
    }

    // Delete all but the two most recent packets
    function cleanPacketHistory() {
        for (var i=clientGame.serverPackets.length-1; i>=0; i--) {
            if (i < clientGame.serverPackets.length-2) {

                clientGame.serverPackets.splice(i, 1);
            }  
        }
    }

    // Calculate how "far in" we are between the last and next server update 
	function calculateNormalValue (networkDelay) {
        var targetGameState = clientGame.serverPackets[clientGame.serverPackets.length-1];
		var normal = (Date.now() - targetGameState.time) / networkDelay; 

		if (normal > 1) {
			return 1;	
		} else {
			return normal;	
		}
	}

    // Linear interpolation math function
    function lerp (pastPosition, targetPosition, normal) {
        return (targetPosition - pastPosition) * normal + pastPosition;	
    }

    // Calculate the angle of 2 points in radians
    function calculateAngle(x1, y1, x2, y2) {
        var deltaX = x2 - x1;
        var deltaY = y2 - y1;
        return Math.atan2(deltaY, deltaX);
    }

    // Public members
    return {
        mainPlayer: { id: 0, playerNumber: 0 }, // The main client player
        otherPlayers: [], // All other players that are not the main player
        allPlayers: [],
        keysDown: [], // Keys currently being held down
        projectiles: [], // All active projectiles
        serverPackets: [], // Packets received from the server 
        running: false, // If the game is currently running 

        // One update cycle
        update: function (delta) {
            readInput();
            updateMainPlayerAnimation();
            updatePendingInstructions();
            updateGameState(delta);
            cleanPacketHistory();

            // Send packet to server
            var packet = preparePacket();
            if (packet) {
                sendPacket(packet);
            }
        },

        // Set the main player's id
        setMainPlayerId: function (id) {
            this.mainPlayer.id = id;
        },

        // Set the game id to match the game on the server
        setGameInfo: function (gameId, playerNumber) {
            this.id = gameId;
            this.mainPlayer.playerNumber = playerNumber;
            this.mainPlayer.setCharacter(playerNumber);
            this.allPlayers.push(this.mainPlayer);
        },

        // Accept a packet from the server
        receivePacket: function (packet) {
            // Mark when we recieve the packet
            packet.time = Date.now();
            this.serverPackets.push(packet);
        },

        // Add a new player to the game
        addNewPlayer: function (player) {
            if (this.mainPlayer.id !== player.id) {
	            // Decorate player with player data
                coreGame.Player(player);
                player.setCharacter(player.playerNumber);
                this.otherPlayers.push(player);
                this.allPlayers.push(player);
            }
        },

        // Remove a player from the game
        removePlayer: function (id) {
            for (var i=this.otherPlayers.length-1; i>=0; i--) {
                if (this.otherPlayers[i].id === id) {

	                this.otherPlayers.splice(i, 1);
                } 
            }
            for (var i=this.allPlayers.length-1; i>=0; i--) {
                if (this.allPlayers[i].id === id) {

	                this.allPlayers.splice(i, 1);
                } 
            }
        }
    };
})();
