"use strict";

var socket = io.connect();

// Set main player id
socket.on('onConnected', function (data) {
    clientGame.setMainPlayerId(data.clientId);
});

// Start the game 
socket.on('startGame', function () {
    if (!clientGame.running) {
        clientGame.running = true;
        gameLoop();
        drawingLoop(drawer);
        $('#start-button').hide();
        $('#messages').html('');
        soundPlayer.playMusic();
    }
});

// Stop the game 
socket.on('gameOver', function (data) {
    switch (data.playerNumber) {
        case 0:
            var winner = 'Cecil';
            break;
        case 1:
            var winner = 'Kain';
            break;
        case 2:
            var winner = 'Rosa';
            break;
        case 3:
            var winner = 'Edge';
            break;
    }
    $('#winner').html(
        'Player ' + (data.playerNumber + 1)+ '</br>' +
        winner + ' won!'
    );
    $('#winner-screen').show();
});
socket.on('removedFromGame', function () {
    $('#messages').html('');
    $('#find-game-button').css('display', 'block');
    $('#start-button').hide();
    $('#canvas').css('background-color', 'grey');
    clientGame.running = false;
    clientGame.otherPlayers = [];
    clientGame.allPlayers = [];
});

// Set other player
socket.on('addNewPlayer', function (data) {
    clientGame.addNewPlayer(data.player);
    $('#messages').append('Player ' + (data.player.playerNumber+1) + ' has joined the game.</br>');
    drawer.drawScoreBoard();

    if (clientGame.otherPlayers.length) {
        $('#start-button').css('display', 'block');
    } else {
        $('#start-button').hide();
    }
});

// Set the game id and player number
socket.on('setGameInfo', function (data) {
    clientGame.setGameInfo(data.gameId, data.playerNumber);
    
    soundPlayer.music.stop();
    $('#winner-screen').hide();
    $('#find-game-button').hide();
    if (clientGame.otherPlayers.length) {
        $('#start-button').css('display', 'block');
    } else {
        $('#start-button').hide();
    }
});

// Remove a player from the game 
socket.on('removePlayer', function (data) {
    clientGame.removePlayer(data.playerId);

    if (clientGame.otherPlayers.length) {
        $('#start-button').css('display', 'block');
    } else {
        $('#start-button').hide();
    }
});

// Receive a game world packet from the server
socket.on('sendPacket', function (data) {
    clientGame.receivePacket(data.packet);
});
