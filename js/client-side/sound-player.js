"use strict";

function SoundPlayer(loader) {
    this.loader = loader

    this.music = this.loader.loadSound("../../audio/customize-my-world");
    this.music.volume = 0.5;
    this.fireSound = this.loader.loadSound("../../audio/smb_fireball");
    this.deathSound = this.loader.loadSound("../../audio/smb_bump");

    // Loop the music
    this.music.addEventListener('ended', function() {
        this.currentTime = 0;
        this.play();
    }, false);

    // Give music a stop function
    this.music.stop = function () {
        this.pause();
        this.currentTime = 0.0;
    }

    this.playMusic = function () {
        this.music.play();
    };

    this.playFireSound = function () {
        this.fireSound.play();
    };

    this.playDeathSound = function () {
        this.deathSound.play();
    };
}
