"use strict";

function Drawer(canvas, scoreBoardCanvas, clientGame) {
    this.canvas = canvas;
    var context = this.canvas.getContext('2d');
    context.imageSmoothingEnabled = false; // Disable smooth sprite scaling

    this.scoreBoardCanvas = scoreBoardCanvas;
    var scoreBoardContext = this.scoreBoardCanvas.getContext('2d');
    scoreBoardContext.imageSmoothingEnabled = false; // Disable smooth sprite scaling
    scoreBoardContext.font = "16px Liberation Serif";

    this.clientGame = clientGame;

    var cecilSheet = loader.loadImage("../../images/cecil.png"); 
    var kainSheet = loader.loadImage("../../images/kain.png"); 
    var rosaSheet = loader.loadImage("../../images/rosa.png"); 
    var edgeSheet = loader.loadImage("../../images/edge.png"); 
    var smbSheet = loader.loadImage("../../images/smb_sheet.png"); 
	var bgImage = loader.loadImage("../../images/background.png");

    this.draw = function () {
        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height);
        scoreBoardContext.clearRect(0, 0, scoreBoardCanvas.width, scoreBoardCanvas.height);

        // Draw background
        context.drawImage(bgImage, 0, 0);

        // Draw main player
        var player = this.clientGame.mainPlayer;
        context.drawImage(
            player.spriteSheet,
            player.currentAnimation[player.frameIndex].sx,
            player.currentAnimation[player.frameIndex].sy,
            player.currentAnimation[player.frameIndex].sw,
            player.currentAnimation[player.frameIndex].sh,
            player.x,
            player.y,
            player.currentAnimation[player.frameIndex].dw,
            player.currentAnimation[player.frameIndex].dh
        );

        // Draw the other players
        for (var i=0; i<this.clientGame.otherPlayers.length; i++) {
            var otherPlayer = this.clientGame.otherPlayers[i];
            context.drawImage(
                otherPlayer.spriteSheet,
                otherPlayer.currentAnimation[otherPlayer.frameIndex].sx,
                otherPlayer.currentAnimation[otherPlayer.frameIndex].sy,
                otherPlayer.currentAnimation[otherPlayer.frameIndex].sw,
                otherPlayer.currentAnimation[otherPlayer.frameIndex].sh,
                otherPlayer.x,
                otherPlayer.y,
                otherPlayer.currentAnimation[otherPlayer.frameIndex].dw,
                otherPlayer.currentAnimation[otherPlayer.frameIndex].dh
            );
        }

        // Draw projectiles
        if (this.clientGame.projectiles) {
            for (var i=0; i<this.clientGame.projectiles.length; i++) {
                context.drawImage(
                    smbSheet,
                    26,
                    165,
                    8,
                    8,
                    this.clientGame.projectiles[i].x,
                    this.clientGame.projectiles[i].y,
                    this.clientGame.projectiles[i].width,
                    this.clientGame.projectiles[i].height
                );
            }
        }

    };

    this.drawScoreBoard = function () {
        // Update the scoreboard
        for (var i=0; i<this.clientGame.allPlayers.length; i++) {
            var player = this.clientGame.allPlayers[i];
            scoreBoardContext.fillText(
                player.characterName,
                player.animations.face.dx + player.animations.face.dw + 10,
                player.animations.face.dy + 15
            );
            scoreBoardContext.drawImage(
                player.spriteSheet,
                player.animations.face.sx,
                player.animations.face.sy,
                player.animations.face.sw,
                player.animations.face.sh,
                player.animations.face.dx,
                player.animations.face.dy,
                player.animations.face.dw,
                player.animations.face.dh
            );
            if (player.id === this.clientGame.mainPlayer.id) {
                scoreBoardContext.fillText(
                    "(You)",
                    player.animations.face.dx + player.animations.face.dw + 50,
                    player.animations.face.dy + 15
                );
            }
            var pointDx = player.animations.face.dx + player.animations.face.dw + 15;
            for (var j=0; j<player.kills; j++) {
                scoreBoardContext.drawImage(
                    smbSheet,
                    26,
                    165,
                    8,
                    8,
                    pointDx,
                    player.animations.face.dy + (player.animations.face.dh / 2) - 8,
                    16,
                    16
                );
                pointDx = pointDx + 26;
            }
        }
    };
}
