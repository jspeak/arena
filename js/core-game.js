"use strict";

// Game code shared by the server and client
var coreGame = {
    networkDelay: 56, // Our target network delay
    projectileId: 0, // Keeps track of projectiles
    gameWidth: 640, // The dimensions of the game world
    gameHeight: 512,
    killsToWin: 10, // Kill count needed to win the game
    initiatedNewRound: false,
    newRoundStarted: false,

    map: {
        impassableTerrain: [
            // Top tree line
            { 
                x: 0,
                y: 0,
                width: 640,
                height: 32 
            },
            { 
                x: (640 / 2) - 32,
                y: 32,
                width: 64,
                height: 32 
            },
            // Bottom tree line
            {
                x: 0,
                y: 512 - 32,
                width: 640,
                height: 32 
            },
            { 
                x: (640 / 2) - 32,
                y: 512 - 64,
                width: 64,
                height: 32 
            },
            // Left tree line
            { 
                x: 0,
                y: 0,
                width: 32,
                height: 640 
            },
            { 
                x: 32,
                y: (512 / 2) - 32,
                width: 32,
                height: 64 
            },
            // Right tree line
            { 
                x: 640 - 32,
                y: 0,
                width: 32,
                height: 640 
            },
            { 
                x: 640 - 64,
                y: (512 / 2) - 32,
                width: 32,
                height: 64 
            },
            // Center trees
            {
                x: (640 / 2) - 32,
                y: (512 / 2) - 32,
                width: 64,
                height: 64 
            }
        ],

        // Check if entity is in impassable terrain
        isInImpassableTerrain: function (entity) {
            for (var i=0; i<this.impassableTerrain.length; i++) {
                var terrain = this.impassableTerrain[i];
                
                if (
                    entity.x + entity.width > terrain.x &&
                    entity.x < terrain.x + terrain.width &&
                    entity.y + entity.height > terrain.y &&
                    entity.y < terrain.y + terrain.height)
                {
                    return true;
                }
            }
            return false;
        }
    },

    // Move a player
    handleInput: function (client, input, delta) {
        if (client.isAlive) {
            switch (input) {
                case 'up':
                    var newPosition = { 
                        x: client.x, 
                        y: client.y - Math.round(client.speed * delta),
                        width: client.width,
                        height: client.height,
                    }; 
                    if (!coreGame.map.isInImpassableTerrain(newPosition)) {
                        client.y -= Math.round(client.speed * delta); 
                    }
                    client.facing = input;
                    client.currentAnimation = client.animations.moveUp;
                    break;
                case 'down':
                    var newPosition = { 
                        x: client.x, 
                        y: client.y + Math.round(client.speed * delta),
                        width: client.width,
                        height: client.height
                    }; 
                    if (!coreGame.map.isInImpassableTerrain(newPosition)) {
                        client.y += Math.round(client.speed * delta);
                    }
                    client.facing = input;
                    client.currentAnimation = client.animations.moveDown;
                    break;
                case 'left':
                    var newPosition = { 
                        x: client.x - Math.round(client.speed * delta), 
                        y: client.y,
                        width: client.width,
                        height: client.height
                    }; 
                    if (!coreGame.map.isInImpassableTerrain(newPosition)) {
                        client.x -= Math.round(client.speed * delta);
                    }
                    client.facing = input;
                    client.currentAnimation = client.animations.moveLeft;
                    break;
                case 'right':
                    var newPosition = { 
                        x: client.x + Math.round(client.speed * delta), 
                        y: client.y,
                        width: client.width,
                        height: client.height
                    }; 
                    if (!coreGame.map.isInImpassableTerrain(newPosition)) {
                        client.x += Math.round(client.speed * delta);
                    }
                    client.facing = input;
                    client.currentAnimation = client.animations.moveRight;
                    break;
                case 'fire':
                    if (client.canFire()) {
                        client.fire();
                    }
                    break;
                default:
                    break;
            }
        }
    },

    handleProjectileMovement: function (client, delta) {
        for (var i=client.projectiles.length-1; i>=0; i--) {
            var projectile = client.projectiles[i];

            // Update the projectile's position
            switch (projectile.direction) {
                case 'up':
                    projectile.y -= Math.round(projectile.speed * delta);
                    break;
                case 'down':
                    projectile.y += Math.round(projectile.speed * delta);
                    break;
                case 'left':
                    projectile.x -= Math.round(projectile.speed * delta);
                    break;
                case 'right':
                    projectile.x += Math.round(projectile.speed * delta);
                    break;
                default:
                    break;
            }
        }
    },

    handleProjectileCollision: function (client, clients) {
        for (var i=client.projectiles.length-1; i>=0; i--) {
            var projectile = client.projectiles[i];

            // Delete the projectile if it hits a wall
            if (coreGame.map.isInImpassableTerrain(projectile)) {
                client.projectiles.splice(i, 1); 
            }

            // If a projectile hits another player
            for (var j=0; j<clients.length; j++) {
                var targetClient = clients[j]; 
                // Make sure the projectile does not belong to the client that fired it, and the hit client is still alive
                if (targetClient.id !== client.id && targetClient.isAlive) {
                    if (
                        projectile.x + projectile.width > targetClient.x &&
                        projectile.x < targetClient.x + targetClient.width &&
                        projectile.y + projectile.height > targetClient.y &&
                        projectile.y < targetClient.y + targetClient.height)
                    {
                        targetClient.health--;
                        // If it was a killing blow
                        if (targetClient.health <= 0) {
                            client.kills++;
                        }
                        client.projectiles.splice(i, 1); 
                    }
                }
            }
        }
    },

    // Reset the game state to begin a new game round after a delay
    startNewRound: function (clients, delay) {
        var self = this;
        setTimeout(function () {
            self.initiatedNewRound = false;
            for (var i=0; i<clients.length; i++) {
                var client = clients[i];

                // Reset player data
                client.x = 0;
                client.y = 0;
                client.health = 1;
                client.isAlive = true;
                client.projectiles = [];
                client.canPlayDeathSound = true;

                // Set spawn point
                switch (i) {
                    case 0: // Player 1
                        client.x = 32;
                        client.y = 32;
                        client.facing = 'right';
                        client.currentAnimation = client.animations.moveRight;
                        break;
                    case 1: // Player 2
                        client.x = coreGame.gameWidth - client.width - 32;
                        client.y = 32;
                        client.facing = 'left';
                        client.currentAnimation = client.animations.moveLeft;
                        break;
                    case 2: // Player 3
                        client.x = 32;
                        client.y = coreGame.gameHeight - client.height - 32;
                        client.facing = 'right';
                        client.currentAnimation = client.animations.moveRight;
                        break;
                    case 3: // Player 4
                        client.x = coreGame.gameWidth - client.width - 32;
                        client.y = coreGame.gameHeight - client.height - 32;
                        client.facing = 'left';
                        client.currentAnimation = client.animations.moveLeft;
                        break;
                }
            }
            self.newRoundStarted = true;
        }, delay);
    },

    // Check if anyone has died
    handleDeaths: function (clients) {
        for (var i=0; i<clients.length; i++) {
            if (clients[i].health <= 0) {
                clients[i].isAlive = false;
                if (clients[i].canPlayDeathSound) {
                    clients[i].playDeathSound = true;
                    // Each client can play the death sound at most once per round
                    clients[i].canPlayDeathSound = false;
                }
            }
        }
    },

    // Start a new round if there's only one player left alive
    checkEndRound: function (clients) {
        var alivePlayersCount = 0;
        for (var i=0; i<clients.length; i++) {
            if (clients[i].isAlive) {
                alivePlayersCount++; 
            }
        }
        if (alivePlayersCount <= 1 && !this.initiatedNewRound) {
            this.initiatedNewRound = true;
            this.startNewRound(clients, 3000);
        }
    },

    // Check if the game has ended
    checkGameOver: function (clients) {
        for (var i=0; i<clients.length; i++) {
            if (clients[i].kills >= this.killsToWin) {
                return clients[i]; 
            }
        }
    },

    // Decorator for player 
    Player: function (player) {
        player.x = 0;
        player.y = 0;
        player.health = 1;
        player.isAlive = true;
        player.kills = 0;
        player.speed = 100; // In px per second
        player.facing = 'down';
        player.width = 32;
        player.height = 32;
        player.projectiles = [];
        player.fireCooldown = 700;
        player.timeOfLastShot = 0;
        player.canPlayDeathSound = true;

        // Animation properties
        player.spriteSheet = '';
        player.cecilAnimations = {
            face: { sx: 4, sy: 13, sw: 32, sh: 32, dx: 5, dy: 5,  dw: 64, dh: 64 },
            moveUp: [
                { sx: 44, sy: 30, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 61, sy: 30, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            moveDown: [
                { sx: 44, sy: 8, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 61, sy: 8, sw: 16, sh: 16, dw: 32, dh: 32 } 
            ],
            moveLeft: [
                { sx: 97, sy: 31, sw: 13, sh: 16, dw: 26, dh: 32 },
                { sx: 83, sy: 31, sw: 13, sh: 16, dw: 26, dh: 32 } 
            ],
            moveRight: [
                { sx: 82, sy: 8, sw: 13, sh: 16, dw: 26, dh: 32 }, 
                { sx: 98, sy: 8, sw: 13, sh: 16, dw: 26, dh: 32 }
            ],
            dead: [
                { sx: 133, sy: 34, sw: 16, sh: 13, dw: 32, dh: 26 },
                { sx: 133, sy: 34, sw: 16, sh: 13, dw: 32, dh: 26 }
            ]
        };
        player.kainAnimations = {
            face: { sx: 6, sy: 9, sw: 32, sh: 32, dx: 5, dy: 79, dw: 64, dh: 64 },
            moveUp: [
                { sx: 49, sy: 25, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 68, sy: 25, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            moveDown: [
                { sx: 49, sy: 4, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 67, sy: 4, sw: 16, sh: 16, dw: 32, dh: 32 } 
            ],
            moveLeft: [
                { sx: 87, sy: 25, sw: 16, sh: 16, dw: 32, dh: 32 },
                { sx: 104, sy: 25, sw: 16, sh: 16, dw: 32, dh: 32 } 
            ],
            moveRight: [
                { sx: 87, sy: 4, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 104, sy: 4, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            dead: [
                { sx: 144, sy: 28, sw: 16, sh: 13, dw: 32, dh: 26 },
                { sx: 144, sy: 28, sw: 16, sh: 13, dw: 32, dh: 26 }
            ]
        };
        player.rosaAnimations = {
            face: { sx: 2, sy: 7, sw: 32, sh: 32, dx: 5, dy: 153, dw: 64, dh: 64 },
            moveUp: [
                { sx: 39, sy: 24, sw: 15, sh: 16, dw: 30, dh: 32 }, 
                { sx: 58, sy: 24, sw: 15, sh: 16, dw: 30, dh: 32 }
            ],
            moveDown: [
                { sx: 39, sy: 4, sw: 15, sh: 16, dw: 30, dh: 32 }, 
                { sx: 57, sy: 4, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            moveLeft: [
                { sx: 77, sy: 4, sw: 15, sh: 16, dw: 30, dh: 32 }, 
                { sx: 96, sy: 4, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            moveRight: [
                { sx: 97, sy: 24, sw: 15, sh: 16, dw: 30, dh: 32 }, 
                { sx: 77, sy: 24, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            dead: [
                { sx: 136, sy: 27, sw: 16, sh: 13, dw: 32, dh: 26 },
                { sx: 136, sy: 27, sw: 16, sh: 13, dw: 32, dh: 26 }
            ]
        };
        player.edgeAnimations = {
            face: { sx: 3, sy: 4, sw: 30, sh: 32, dx: 5, dy: 227, dw: 60, dh: 64 },
            moveUp: [
                { sx: 38, sy: 21, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 58, sy: 21, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            moveDown: [
                { sx: 37, sy: 1, sw: 16, sh: 16, dw: 32, dh: 32 }, 
                { sx: 57, sy: 1, sw: 16, sh: 16, dw: 32, dh: 32 } 
            ],
            moveLeft: [
                { sx: 78, sy: 1, sw: 15, sh: 16, dw: 30, dh: 32 },
                { sx: 98, sy: 1, sw: 16, sh: 16, dw: 32, dh: 32 } 
            ],
            moveRight: [
                { sx: 100, sy: 21, sw: 15, sh: 16, dw: 30, dh: 32 }, 
                { sx: 79, sy: 21, sw: 16, sh: 16, dw: 32, dh: 32 }
            ],
            dead: [
                { sx: 138, sy: 25, sw: 16, sh: 12, dw: 32, dh: 24 },
                { sx: 138, sy: 25, sw: 16, sh: 12, dw: 32, dh: 24 }
            ]
        };
        player.animations = player.cecilAnimations;
        player.currentAnimation = player.animations.moveRight;
        player.tickCount = 0;
        player.ticksPerSprite = 15;
        player.frameIndex = 0;

        player.setCurrentAnimation = function (animation) {
           player.currentAnimation = animation;
        }

        // Advance the current animation of the player
        player.advanceFrameIndex = function () {
            if (player.tickCount > player.ticksPerSprite) {
                player.tickCount = 0;
                if (player.frameIndex >= player.currentAnimation.length-1) {
                    player.frameIndex = 0;	
                } else {
                    player.frameIndex++;
                }
            } else {
                player.tickCount++;	
            }
        }

        player.setCharacter = function (playerNumber) {
            switch (playerNumber) {
                case 0:
                    if (coreGame.isClientSide()) {
                        player.spriteSheet = loader.loadImage("../../images/cecil.png");
                    }
                    player.animations = player.cecilAnimations;
                    player.characterName = "Cecil";
                    break;
                case 1:
                    if (coreGame.isClientSide()) {
                        player.spriteSheet = loader.loadImage("../../images/kain.png");
                    }
                    player.animations = player.kainAnimations;
                    player.characterName = "Kain";
                    break;
                case 2:
                    if (coreGame.isClientSide()) {
                        player.spriteSheet = loader.loadImage("../../images/rosa.png");
                    }
                    player.animations = player.rosaAnimations;
                    player.characterName = "Rosa";
                    break;
                case 3:
                    if (coreGame.isClientSide()) {
                        player.spriteSheet = loader.loadImage("../../images/edge.png");
                    }
                    player.animations = player.edgeAnimations;
                    player.characterName = "Edge";
                    break;
            } 
        }

        // Projectile object
        player.Projectile = function () {
            this.id = coreGame.projectileId++;
            this.speed = 250;
            this.direction = player.facing;
            this.width = 16;
            this.height = 16;

            // Determine the spawn point of the projectile
            switch (this.direction) {
                case 'up':
                    this.x = player.x + (player.width / 2) - (this.width / 2); 
                    this.y = player.y - (this.height); 
                    break;
                case 'down':
                    this.x = player.x + (player.width / 2) - (this.width / 2); 
                    this.y = player.y + player.height; 
                    break;
                case 'left':
                    this.x = player.x - this.width; 
                    this.y = player.y + (player.height / 2) - (this.height / 2);
                    break;
                case 'right':
                    this.x = player.x + player.width; 
                    this.y = player.y + (player.height / 2) - (this.height / 2);
                    break;
             }
        }

        // Check if player is able to fire
        player.canFire = function () {
            if ((Date.now() - player.timeOfLastShot) > player.fireCooldown) {
                return true;
            }

            return false;
        }

        // Fire a projectile
        player.fire = function () {
            player.timeOfLastShot = Date.now();
            var projectile = new player.Projectile();
            player.projectiles.push(projectile);
            
            // Play sound if client-side
            if (coreGame.isClientSide()) { 
                soundPlayer.playFireSound();
            } else {
                player.playFireSound = true;
            }
        }
    },

    // Neat trick to check if we are server or client side
    isClientSide: function () {
        if (typeof(global) === 'undefined') { 

            return true;
        }
    }
}

if (!coreGame.isClientSide()) { 
    module.exports = coreGame;
}
