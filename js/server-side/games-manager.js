"use strict";

var Game = require('./server-game.js');

// Games Manager 
module.exports = {
    games: [],
    uniqueGameId: 0, // The id we use for the next game created
    runGames: false, // Tells us if we should be running the game loops

    // Update all running games
    updateGames: function (delta) {
        if (this.games.length) {
            for (var i=0; i<this.games.length; i++) {
                // Update the game
                this.games[i].update(delta);

                // Delete the game if it's empty or it has ended
                if (this.games[i].clients.length === 0 || this.games[i].gameOver) {
                    this.removeGame(this.games[i]);
                }
            }
        } else {
            this.runGames = false;
        }
    },

    // Broadcast the game state to each client for all games
    broadcastGameStates: function () {
        for (var i=0; i<this.games.length; i++) {
            this.games[i].broadcastGameState();
        }
    },

    // Find an open game or create a new one if no game is available
    findGame: function () {
        // Attempt to find game that has not started yet, and has less than 4 players
        for (var i=0; i<this.games.length; i++) {
            if (!this.games[i].inProgress && this.games[i].clients.length < 4) {

                return this.games[i];
            }
        }
        
        // Create new game if no open games available
        return this.createGame();
    },

    // Create a new game
    createGame: function () {
        // Increment the uniqueGameId to ensure all games have a unique id
        var newGame = new Game(this.uniqueGameId++);
        this.games.push(newGame);

        return newGame;
    },

    // Delete a game
    removeGame: function (game) {
        for (var i=this.games.length-1; i>=0; i--) {
            if (game === this.games[i]) {

                // Remove all clients from the game first
                for (var j=this.games[i].clients.length-1; j>=0; j--) {
                   this.games[i].removeClient(this.games[i].clients[j]);       
                }
                
                this.games.splice(i, 1); 
            }
        }        
    },

    // Tell all relevant clients when a player has joined their game
    notifyClientsPlayerJoined: function (game, client) { 
        var newPlayer = {
            id: client.id,
            x: client.x,
            y: client.y,
            playerNumber: game.clients.length
        };

        for (var i=0; i<game.clients.length; i++) {
	        game.clients[i].emit('addNewPlayer', { player: newPlayer }); 
        }
    },

    // Tell all relevant clients when a player has left their game
    notifyClientsPlayerLeft: function (game, client) { 
        for (var i=0; i<game.clients.length; i++) {
	        game.clients[i].emit('removePlayer', { playerId: client.id }); 
        }
    },
}
