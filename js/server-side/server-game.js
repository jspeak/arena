"use strict";

var coreGame = require('../core-game.js');

// Server specific game code
module.exports = function (id) {
    return {
        id: id,
        clients: [], // List of clients connected to the game
        pendingPackets: [], // Packets yet to be executed
        packetHistory: [], // History of previously executed packets
        inProgress: false, // If the game is in progress 
        gameOver: false, // If the game has ended

        // One game update cylce
        update: function (delta) {
            this.cleanPacketHistory(2000);

            // Set client's lastInstructionSequenceNumber
            for (var i=0; i<this.clients.length; i++) {
                this.setClientLisn(this.clients[i]);
            }

            // Run through all of our pending packets
            for (var i=this.pendingPackets.length-1; i>=0; i--) {
                var packet = this.pendingPackets[i];
                var client = this.getClientById(packet.clientId);

                // Execute instructions for each pending packet
                for (var j=0; j<packet.instructions.input.length; j++) {
                    if (client) { // Not sure why we have to do this check
                        coreGame.handleInput(client, packet.instructions.input[j], delta);

                        // Mark the last input we've processed for the client     
                        client.lastInstructionSequenceNumber = packet.instructions.isn;
                    }
                }

                // Update our packet history
                this.packetHistory.push(packet);
                this.pendingPackets.splice(i, 1);
            } 

            // Handle projectiles
            for (var i=0; i<this.clients.length; i++) {
                coreGame.handleProjectileMovement(this.clients[i], delta);
                coreGame.handleProjectileCollision(this.clients[i], this.clients);
            }

            coreGame.handleDeaths(this.clients);
            coreGame.checkEndRound(this.clients);

            // Check if game has ended
            var winner = coreGame.checkGameOver(this.clients);
            if (winner) {
                for (var i=0; i<this.clients.length; i++) {
                    this.clients[i].emit('gameOver', { playerNumber: winner.playerNumber });
                }
                // End the game after a small delay
                var self = this;
                setTimeout(function () {
                    self.gameOver = true;
                }, 1000);
            }
        },

        // Start the game
        startGame: function () {
            this.inProgress = true;
            coreGame.startNewRound(this.clients, 0);

            for (var i=0; i<this.clients.length; i++) {
                this.clients[i].emit('startGame');
            }
        },

        // Broadcast the game state to all clients in the game
        broadcastGameState: function () {
            var packet = this.preparePacket();

            for (var i=0; i<this.clients.length; i++) {
                this.clients[i].emit('sendPacket', { packet: packet });
            } 
        },

        // Return all the important details about our game world
        preparePacket: function () {
            var packet = {
                players: [],
                projectiles: [],
                //time: Date.now()
            };

            for (var i=0; i<this.clients.length; i++) {
                var playerData = {
                    id: this.clients[i].id,
                    x: this.clients[i].x,
                    y: this.clients[i].y,
                    kills: this.clients[i].kills,
                    isAlive: this.clients[i].isAlive,
                    currentAnimation: this.clients[i].currentAnimation,
                    lastInstructionSequenceNumber: this.clients[i].lastInstructionSequenceNumber
                }
                
                // Handle sounds
                if (this.clients[i].playFireSound) {
                    playerData.playFireSound = true;
                    this.clients[i].playFireSound = false;
                }
                if (this.clients[i].playDeathSound) {
                    playerData.playDeathSound = true;
                    this.clients[i].playDeathSound = false;
                }

                packet.players.push(playerData);

                // Handle projectiles
                for (var j=0; j<this.clients[i].projectiles.length; j++) {
                    packet.projectiles.push(
                        this.clients[i].projectiles[j]
                    );
                }

                // Let the clients know if it's the start of a new round
                if (coreGame.newRoundStarted) {
                    packet.newRoundStarted = true;
                    coreGame.newRoundStarted = false;
                }
            }
            
            return packet;   
        },

        // Add a client to the game
        addClient: function (client) {
            client.playerNumber = this.clients.length;
            client.setCharacter(client.playerNumber);
            this.clients.push(client);     

            // Tell the client the game id and what player they are
	        client.emit('setGameInfo', { gameId: this.id, playerNumber: client.playerNumber }); 

            // Tell the client about the other players in game 
            for (var i=0; i<this.clients.length; i++) {
                var player = {
                    id: this.clients[i].id,
                    x: this.clients[i].x,
                    y: this.clients[i].y,
                    playerNumber: this.clients[i].playerNumber
                };
                
                client.emit('addNewPlayer', { player: player }); 
            }
        },

        // Remove a client from the game
        removeClient: function (client) {
            for (var i=this.clients.length-1; i>=0; i--) {
                
                if (this.clients[i].id === client.id) {
                    // Reset player data
                    coreGame.Player(client);
                    client.game = null;

                    // Remove the client
                    this.clients.splice(i, 1);
                    client.emit('removedFromGame');
                }
            }
        },

        // Receive a packet from a client
        receiveClientPacket: function (packet) {
            if (this.packetIsValid(packet)) {

                this.pendingPackets.push(packet);   
            }
        },
        
        // Check if the packet is valid 
        packetIsValid: function (packet) {
            // Get the last packet received from the client
            for (var i=this.packetHistory.length-1; i>=0; i--) {
                if (this.packetHistory[i].clientId === packet.clientId) {
                    
                    var lastPacketFromClient = this.packetHistory[i];
                    break;
                }
            }
            // We also have to check in pendingPackets since it's possible it could be here
            for (var i=this.pendingPackets.length-1; i>=0; i--) {
                if (
                    this.pendingPackets[i].clientId === packet.clientId &&
                    (lastPacketFromClient && this.pendingPackets[i].time > lastPacketFromClient.time)) 
                {
                        lastPacketFromClient = this.pendingPackets[i];
                        break;
                }
            }
            // Make sure it's been at least 16ms since last packet
            if (lastPacketFromClient) {
                if ((packet.time - lastPacketFromClient.time) < 16) {
                    
                    return false;
                }  
            }             

            // Make sure there is a most one of each possible input in the packet
            packet.instructions.input.sort(); 
            for (var i=0; i<packet.instructions.input.length; i++) {
                if (packet.instructions.input[i] == packet.instructions.input[i+1]) {

                    return false;                 
                }
            }

            return true
        },
        
        // Set the client's lisn based on their last recieved packet 
        setClientLisn: function (client) {
            for (var i=this.pendingPackets.length-1; i>=0; i--) {
                if (this.pendingPackets[i].clientId === client.id) {
                    
                    client.lastInstructionSequenceNumber = this.pendingPackets[i].instructions.isn; 
                    return;
                }
            }    
        },

        // Return a client with id clientId
        getClientById: function (clientId) {
            for (var i=0; i<this.clients.length; i++) {
                if (this.clients[i].id === clientId) {

                    return this.clients[i];	
                }
            }
        },
        
        // Remove packets from packetHistory older than the specified time
        cleanPacketHistory: function (time) {
            for (var i=this.packetHistory.length-1; i>=0; i--) {
                if (this.packetHistory[i].time < (Date.now() - time)) {
                    this.packetHistory.splice(i, 1);
                } 
            }
        }
    };
}
